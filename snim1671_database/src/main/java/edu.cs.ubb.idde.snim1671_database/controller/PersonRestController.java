package edu.cs.ubb.idde.snim1671_database.controller;


import edu.cs.ubb.idde.snim1671_database.model.Person;
import edu.cs.ubb.idde.snim1671_database.persistence.PersonRepository;
import edu.cs.ubb.idde.snim1671_database.util.DTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/persons")
public class PersonRestController {
    private PersonRepository personRepository;

    public PersonRestController(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @GetMapping
    public List<Person> getPersons() {
        return personRepository.findAll();
    }

    @PostMapping
    public void newPerson(@DTO(edu.cs.ubb.idde.snim1671_database.model.dto.PersonCreationDTO.class) Person person) {
        personRepository.save(person);
    }

}