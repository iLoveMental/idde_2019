package edu.cs.ubb.idde.snim1671_database.persistence;


import edu.cs.ubb.idde.snim1671_database.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person, Long> {
}
