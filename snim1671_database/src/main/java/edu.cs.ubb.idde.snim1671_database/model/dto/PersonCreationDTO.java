package edu.cs.ubb.idde.snim1671_database.model.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PersonCreationDTO {
    @NotNull
    private String name;

    @NotNull
    private String address;

    @NotNull
    private String age;

    @NotNull
    private String phoneNumber;



}
