package edu.cs.ubb.idde.snim1671_database.model;

import lombok.Data;

import javax.persistence.*;
@Data
@Entity
@Table(name = "persons")

public class Person {
    private String idPerson;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String name;
    @Column(name = "Name")
    private String age;
    @Column(name = "Age")
    private String address;
    @Column(name = "Address")
    private String phoneNumber;
    @Column(name = "PhoneNumber")

    public String getId() {   return idPerson;  }

    public String getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setId(String Id) {  this.idPerson = Id;  }

    public void setAge(String Age) {
        this.age = Age;
    }

    public void setAddress(String Address) {
        this.address = Address;
    }

    public void setName(String Name) {
        this.name = Name;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}